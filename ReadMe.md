#DocRefRegistry

A FHIR compliant document registry using HAPI RESTFul server components.

Licensed under the Apache License, Version 2.0

## Description

This is a document registry, with a FHIR compliant API, based on the FHIR DSTU2
DocumentReference resource (but profiled resources hopefully coming soon).

The data can be backed off into a choice of database including (so far); MongoDB
RethinkDB or a Stub in memory datastore suitable only for low level testing.


## Purpose

This is a proof of concept, to achieve a number of goals, including some
'playing' with the HAPI Java FHIR helper code.
Another key goal is to provide a test harness against which calls can be made,
to prove how a FHIR based document registry would / should behave.