/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.resources;

import ca.uhn.fhir.model.dstu2.composite.AddressDt;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.BaseResource;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference.Content;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference.Context;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import ca.uhn.fhir.model.dstu2.valueset.DocumentReferenceStatusEnum;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.primitive.UriDt;
import java.util.Date;
import java.util.UUID;

/**
 * A really simple class used to generate Fake resources, mostly used in Unit
 * Testing.
 * 
 * @author Tim Coates
 */
public class ResourceFactory {

    public static BaseResource MakeFake(Class type) {
        BaseResource returnObject = null;
        
        
        //<editor-fold defaultstate="collapsed" desc="Make a DocumentReference">
        if(type.getName().equals("ca.uhn.fhir.model.dstu2.resource.DocumentReference")) {
            DocumentReference dr = new DocumentReference();
            
            dr.setCreated(new DateTimeDt(new Date()));
            dr.setDescription("Fake DocumentReference created for test purposes.");
            
            //<editor-fold defaultstate="collapsed" desc="Add Content">
            Content ct = new Content();
            AttachmentDt att = new AttachmentDt();
            att.setContentType("text/html");
            att.setUrl("http://data.developer.nhs.uk/fhir/nrls-v1-draft-a/Chapter.1.About/index.html");
            att.setSize(11402);
            att.setTitle("NRLS Record Locator Service - FHIR Implementation Guide");
            ct.setAttachment(att);
            dr.addContent(ct);
            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="Add MasterIdentifier">
            dr.setMasterIdentifier(new IdentifierDt("urn:ietf:rfc:3986", UUID.randomUUID().toString()));
            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="Add Subject">
            ResourceReferenceDt subj = new ResourceReferenceDt();
            subj.setReference("http://www.fictional.nhs.uk/fhirserver/Patient/ABC123");
            subj.setDisplay("Mrs P Patient");
            dr.setSubject(subj);
            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="Add Type">
            CodingDt docType = new CodingDt();
            docType.setSystem("http://snomed.info/sct")
                    .setCode("861421000000109")
                    .setDisplay("End of Life Care Coordination Summary");
            dr.setType(new CodeableConceptDt().addCoding(docType));
            //</editor-fold>
            
            // Add Author
            dr.addAuthor().setReference("http://www.fictional.nhs.uk/fhirserver/Practitioner/12121212").setDisplay("Dr. S. Honeyman");
            
            //<editor-fold defaultstate="collapsed" desc="Add Custodian">
            ResourceReferenceDt cust = new ResourceReferenceDt();
            cust.setReference("http://www.fictional.nhs.uk/fhirserver/Organisation/99");
            cust.setDisplay("Silverdale Family Practice");
            dr.setCustodian(cust);
            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="Add Authenticator">
            ResourceReferenceDt authent = new ResourceReferenceDt();
            authent.setReference("http://www.fictional.nhs.uk/fhirserver/Practitioner/007");
            authent.setDisplay("Professor SW Varley");
            dr.setAuthenticator(authent);
            //</editor-fold>
            
            // Set Status
            dr.setStatus(DocumentReferenceStatusEnum.CURRENT);
            
            //<editor-fold defaultstate="collapsed" desc="Add Security Label">
            dr.addSecurityLabel()
                    .addCoding()
                    .setSystem("http://hl7.org/fhir/ValueSet/security-labels")
                    .setCode("V")
                    .setDisplay("very restricted");
            //</editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="Add Context and Facility Type">
            CodeableConceptDt facility = new CodeableConceptDt();
            facility.addCoding()
                    .setSystem("http://snomed.info/sct")
                    .setCode("83891005")
                    .setDisplay("Solo practice private office");
            Context ctx = new Context();
            ctx.setFacilityType(facility);
            dr.setContext(ctx);
            dr.setContext(ctx);
            //</editor-fold>
            
            returnObject = dr;
        }
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="Make an Organization">
        if(type.getName().equals("ca.uhn.fhir.model.dstu2.resource.Organization")) {
            Organization org = new Organization();
            
            // Set an identifier
            org.addIdentifier();
            org.getIdentifier().get(0).setSystem(new UriDt("http://ods.nhs.uk/Organization"));
            org.getIdentifier().get(0).setValue("ABCD1234");
            
            // Set as Active
            org.setActive(true);
            
            // Set type: https://www.hl7.org/fhir/organization-definitions.html#Organization.type
            // TODO:
            
            // Set name
            org.setName("Bogus GP Practice");
            
            // Add an address
            AddressDt addr = new AddressDt();
            addr.addLine("1 Streety Street");
            addr.addLine("Townsville");
            addr.addLine("East County");
            addr.setPostalCode("LS1 4HR");
            org.addAddress(addr);

            // Add a Contact
            // TODO:

            returnObject = org;
        }
        //</editor-fold>
        
        return returnObject;
    }
}
