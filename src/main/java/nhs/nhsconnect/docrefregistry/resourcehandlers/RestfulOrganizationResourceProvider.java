/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.resourcehandlers;

import ca.uhn.fhir.model.dstu2.resource.Organization;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.mongodb.BasicDBObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nhs.nhsconnect.docrefregistry.datastore.iHAPIDatastore;
import nhs.nhsconnect.docrefregistry.resources.ResourceFactory;
import org.hamcrest.core.IsNull;

/**
 * This is the IResourceProvider class for handling Organization resource types.
 * 
 * @author Tim Coates
 */
public class RestfulOrganizationResourceProvider implements IResourceProvider {
    private static final Logger LOG = Logger.getLogger(RestfulOrganizationResourceProvider.class.getName());
    iHAPIDatastore myDatastore = null;

    /**
     * Constructor, which allows the server to give us the nod which datastore
     * we should be reading from and writing to.
     *
     *
     * @param theDatastore
     */
    public RestfulOrganizationResourceProvider(iHAPIDatastore theDatastore) {
        LOG.info("Creating RestfulOrganizationResourceProvider object with datastore: " + theDatastore.GetName());
        myDatastore = theDatastore;
    }

    @Override
    public Class<Organization> getResourceType() {
        return Organization.class;
    }

    /**
     * The "@Read" annotation indicates that this method supports the read
     * operation. Read operations should return a single resource instance.
     *
     * @param theId The read operation takes one parameter, which must be of
     * type IdDt and must be annotated with the "@Read.IdParam" annotation.
     * @return Returns a resource matching this identifier, or null if none
     * exists.
     */
    @Read()
    public Organization getResourceById(@IdParam IdDt theId) {
        LOG.log(Level.INFO, "RestfulOrganizationResourceProvider.getResourceById({0} called", theId.getIdPart());
        String guid = theId.getIdPart();
        LOG.info("We're searching on id for: " + guid);
        Organization thisOrg = myDatastore.getOrganization(guid);
        if(thisOrg == null) {
            LOG.info("Got null back, need to generate a 404?");
        } else {
            LOG.info("Got an Org back");
        }
        LOG.log(Level.INFO, "Returning Organization: {0}", thisOrg.getName());
        return thisOrg;
    }
    
    @Search()
    public List<Organization> getAllOrgs() {
        LOG.info("RestfulOrganizationResourceProvider.getAllOrgs() called ");
        List<Organization> orgList = myDatastore.getAllOrgs();
        return orgList;
    }

    // Here we need to map some search query parameters to calls to
    /**
     * This searches on two identifier URL forms to find by ODS Code:
     *   ?identifier=A81006
     *   ?identifier=http://ods.nhs.uk/Organization|A81006
     * @param ODSIdentifier Can either be a fully specified or a simple ODS Code
     * @return 
     */
    @Search()
    public List<Organization> getMatchingOrgsByODSCode(@RequiredParam(name=Organization.SP_IDENTIFIER) StringParam ODSIdentifier) {
        LOG.info("RestfulOrganizationResourceProvider.getOrgsByODSCode() called ");
        String suppliedIdentifier = ODSIdentifier.getValue();
        String ODSCode = "";
        CharSequence cs1 = "|";
        if(suppliedIdentifier.contains(cs1)) {
            // We're looking for a fully specified type eg:
            // http://ods.nhs.uk/Organization|A81006
            ODSCode = suppliedIdentifier.substring(suppliedIdentifier.indexOf("|")+1);
        } else {
            // We're just looking for the code value eg:
            // A81006
            ODSCode = suppliedIdentifier;
        }
        List<Organization> orgList = myDatastore.getOrgsByODSCode(ODSCode);
        return orgList;
    }
    
    @Search()
    public List<Organization> getMatchingOrgsByAddress(@RequiredParam(name=Organization.SP_ADDRESS) StringParam addressPart) {
        LOG.info("RestfulOrganizationResourceProvider.getMatchingOrgsByAddress() called ");
        List<Organization> orgList = null;
        if(addressPart.isContains()){
            String suppliedAddress = addressPart.getValue();
            LOG.info("Searching on Address contains: " + suppliedAddress);
            orgList = myDatastore.getOrgsByAddress(suppliedAddress);
        }
        return orgList;
    }
}
