/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.resourcehandlers;

import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.OperationOutcome;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import nhs.nhsconnect.docrefregistry.datastore.iHAPIDatastore;

/**
 * This is the class which handles any requests for DocumentReference resources
 *
 * There is a method for each of the REST interactions we support. The server
 * uses the annotations in here both to route calls, but also to generate a
 * Conformance resource which shows what interactions are supported for which
 * resources.
 * 
 * @author Tim Coates
 */
public class RestfulDocumentReferenceResourceProvider implements IResourceProvider {
    private static final Logger LOG = Logger.getLogger(RestfulDocumentReferenceResourceProvider.class.getName());
    
    iHAPIDatastore myDatastore = null;
    
    /**
     * Constructor, which allows the server to give us the nod which datastore
     * we should be reading from and writing to.
     * 
     * 
     * @param theDatastore 
     */
    public RestfulDocumentReferenceResourceProvider(iHAPIDatastore theDatastore) {
        LOG.info("Creating RestfulDocumentReferenceResourceProvider object with datastore: " + theDatastore.GetName());
        myDatastore = theDatastore;
    }

    /**
     * The getResourceType method comes from IResourceProvider, and must be
     * overridden to indicate what type of resource this provider supplies.
     */
    @Override
    public Class<DocumentReference> getResourceType() {
        return DocumentReference.class;
    }

    /**
     * Code to get a specific DocRef identified by Identifier...
     * 
     * @param theId ID of the DocRef requested.
     * @return 
     */
    @Read()
    public DocumentReference getResourceById(@IdParam IdDt theId) {
        DocumentReference docRef = myDatastore.getDocRef(theId.getIdPart());
        if(docRef != null)
            LOG.fine("Got DocRef from datastore");
        else
            LOG.fine("Got NO DocRef from datastore");
        return docRef;
    }

    /**
     * Method which is wired up to handle type level GET requests, ie those to
     * [base]/DocumentReference
     * 
     * @return 
     */
    @Search()
    public List<DocumentReference> getAllDocRefs() {
        List<DocumentReference> docRefs = myDatastore.getAllDocRefs();
        return docRefs;
    }
    
    /**
     * Method to handle GET where a ?subject=xyz&type=xyz calls.
     * 
     * @param theSubject
     * @param theType
     * @return 
     */
    @Search()
    public List<DocumentReference> searchDocRefs(
                            @RequiredParam(name=DocumentReference.SP_SUBJECT) StringParam theSubject,
                            @OptionalParam(name=DocumentReference.SP_TYPE)  StringParam theType ) {
        LOG.info("RestfulDocumentReferenceResourceProvider.searchDocRefs() called to search by...");
        List<DocumentReference> foundList = new ArrayList<DocumentReference>();
        if(theType==null) {
            LOG.info("  ... subject");
            foundList = myDatastore.searchDocRefs(theSubject.getValue());
            LOG.info("Found: " + foundList.size() + " records.");
        } else {
            LOG.info("  ... subject AND type");
            foundList = myDatastore.searchDocRefs(theSubject.getValue(), theType.getValue());
        }
        return foundList;
    }

    /**
     * Method which is hooked to the POST call to create a new DocRef.
     * 
     * @param theDocumentReference
     * @return 
     */
    @Create
    public MethodOutcome createDocumentReference(@ResourceParam DocumentReference theDocumentReference) {
        MethodOutcome retVal = new MethodOutcome();
        OperationOutcome outcome = new OperationOutcome();

        theDocumentReference.setId(UUID.randomUUID().toString());
        IdDt newId = new IdDt(myDatastore.saveDocRef(theDocumentReference));
        outcome.setId(newId);
        retVal.setId(newId);
        retVal.setOperationOutcome(outcome);
        return retVal;
    }
}
