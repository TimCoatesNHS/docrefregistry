/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry;

import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import nhs.nhsconnect.docrefregistry.datastore.DATASTORETYPES;
import nhs.nhsconnect.docrefregistry.datastore.DataStoreFactory;
import nhs.nhsconnect.docrefregistry.datastore.iHAPIDatastore;
import nhs.nhsconnect.docrefregistry.resourcehandlers.RestfulDocumentReferenceResourceProvider;
import nhs.nhsconnect.docrefregistry.resourcehandlers.RestfulOrganizationResourceProvider;

/**
 * This is effectively the core of a HAPI RESTFul server.
 * 
 * We create a datastore in initialize method, which we pass to each ResourceProvider
 * so that all resources can be persisted to/from the same datastore.
 * 
 * @author Tim Coates
 */
@WebServlet(urlPatterns = {"/FHIR/*"}, displayName = "FHIR")
public class RestfulServlet extends RestfulServer {
    private static final Logger LOG = Logger.getLogger(RestfulServlet.class.getName());
    private static final long serialVersionUID = 1L;
    iHAPIDatastore ds;
    
    /**
    * This is where we start, called when our servlet is first initialised.
    * For simplicity, we do the datastore setup once here.
    * 
    * 
    * @throws ServletException 
    */
    @Override
    protected void initialize() throws ServletException {
        LOG.info("\n====================\nServlet initialising\n====================");

        ds = DataStoreFactory.MakeDatastore(DATASTORETYPES.MONGO, Settings.MongoHost(), Settings.MongoPort());
        
        if (ds == null) {
            System.exit(4);
        } else {
            LOG.info("Datastore created!");
        }
        //</editor-fold>
        //String serverBaseUrl = "http://localhost:8080/DocRefRegistry/FHIR";
        //setServerAddressStrategy(new HardcodedServerAddressStrategy(serverBaseUrl));

        // Set the Resource Providers we're working with, each of these handles
        // a specific FHIR resource type.
        List<IResourceProvider> resourceProviders = new ArrayList<IResourceProvider>();
        resourceProviders.add(new RestfulDocumentReferenceResourceProvider(ds));
        resourceProviders.add(new RestfulOrganizationResourceProvider(ds));
        
        setResourceProviders(resourceProviders);
        LOG.info("resourceProviders added");
    }
}
