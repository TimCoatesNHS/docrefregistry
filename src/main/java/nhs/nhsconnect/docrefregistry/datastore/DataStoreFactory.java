/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.datastore;

/**
 * This is the factory class for generating a given type of datastore.
 * 
 * @author Tim Coates
 */
public class DataStoreFactory {

    public static iHAPIDatastore ds = null;
    //static String rethinkHost = "localhost";
    //static int rethinkPort = 28015;
    static String mongoHost = "155.231.220.40";
    static int mongoPort = 80;
    
    /**
     * Proper factory here, which allows for more than two types of datastore.
     * 
     * @param Type The type of datastore selected from DATASTORETYPES
     * 
     * @return Returns an implementation of the iHAPIDatastore interface.
     */
    public static iHAPIDatastore MakeDatastore(DATASTORETYPES Type) {
        
        switch(Type) {
            //case RETHINK:
                //ds = new RethinkDatastore(rethinkHost, rethinkPort);
                //break;
            case MONGO:
                ds = new mongoDatastore(mongoHost, mongoPort);
                break;
            case STUB:
            default:
                ds = new StubDatastore();
                break;
        }
        return ds;
    }
    
    /**
     * Proper factory here, which allows for more than two types of datastore.
     * 
     * @param Type The type of datastore selected from DATASTORETYPES
     * @param hostname
     * @param port
     * 
     * @return Returns an implementation of the iHAPIDatastore interface.
     */
    public static iHAPIDatastore MakeDatastore(DATASTORETYPES Type, String hostname, int port) {
        
        switch(Type) {
            //case RETHINK:
            //    ds = new RethinkDatastore(hostname, port);
            //    break;
            case MONGO:
                ds = new mongoDatastore(hostname, port);
                break;
            case STUB:
            default:
                ds = new StubDatastore();
                break;
        }
        return ds;
    }
}
