/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.datastore;

import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import java.util.List;

/**
 *
 * @author Tim Coates
 */
public interface iHAPIDatastore {
    
    // Get the name of the actual datastore (eg Stub or Rethink, or Mongo etc)
    String GetName();
    
//<editor-fold defaultstate="collapsed" desc="Handle DocumentReference calls">
    String saveDocRef(DocumentReference thisone);
    String updateDocRef(DocumentReference thisone);
    DocumentReference getDocRef(String id);
    boolean deleteDocRef(String id);
    public List<DocumentReference> searchDocRefs(String subjectID);
    public List<DocumentReference> searchDocRefs(String subjectID, String type);
    public List<DocumentReference> getAllDocRefs();
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Handle Organization calls">
    /**
     * Gets a single identified Organization from the datastore
     * @param id
     * @return
     */
    Organization getOrganization(String id);
    public List<Organization> getAllOrgs();
    public List<Organization> searchOrgs(String matchValue);
    public List<Organization> getOrgsByODSCode(String ODSCode);
    public List<Organization> getOrgsByAddress(String Address);
//</editor-fold>

}
