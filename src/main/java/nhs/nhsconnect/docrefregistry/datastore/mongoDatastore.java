/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.datastore;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import ca.uhn.fhir.model.primitive.IdDt;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.types.ObjectId;

/**
 * MongoDB datastore implementation.
 *
 * @author Tim Coates
 */
public class mongoDatastore implements iHAPIDatastore {

    private static final Logger LOG = Logger.getLogger(mongoDatastore.class.getName());
    MongoClient mongoClient;
    DB db;
    DBCollection docRefCollection;
    DBCollection orgCollection;
    FhirContext ctx;

    /**
     * Constructor, which creates our MongoDB connection.
     *
     * @param host The host where MongoDB can be found
     * @param port The port we connect to.
     */
    public mongoDatastore(String host, int port) {
        mongoClient = new MongoClient(host, port);
        db = mongoClient.getDB("mydb");
        docRefCollection = db.getCollection("DocumentReferences");
        orgCollection = db.getCollection("Organizations");
        ctx = FhirContext.forDstu2();
    }

    /**
     * Method to find out which type of datastore we're using.
     *
     * @return Returns a name of this datastore type.
     */
    @Override
    public String GetName() {
        LOG.fine("mongoDatastore.GetName() called");
        return "MongoDB";
    }

    /**
     * Code to save a provided DocRef into MongoDB
     *
     * @param thisone
     * @return
     */
    @Override
    public String saveDocRef(DocumentReference thisone) {
        UUID newReference = UUID.randomUUID();

        thisone.setId(new IdDt(newReference.toString()));
        String serialised = ctx.newXmlParser().encodeResourceToString(thisone);
        String patient = thisone.getSubject().getReference().toString();
        String docTypeCode = thisone.getType().getCoding().get(0).getCode();
        String docTypeFull = thisone.getType().getCoding().get(0).getSystem() + "|" + thisone.getType().getCoding().get(0).getCode();

        BasicDBObject doc = new BasicDBObject("_id", newReference.toString())
                .append("content", serialised)
                .append("subject", patient)
                .append("typecode", docTypeCode)
                .append("typefull", docTypeFull);
        docRefCollection.insert(doc);

        return newReference.toString();
    }

    /**
     * Replace a specified record with a new one...
     *
     * @param thisone The replacement DocRef object
     * @return The identifier of the new record, should be the same as was in the supplied one
     */
    @Override
    public String updateDocRef(DocumentReference thisone) {

        String identifier = thisone.getId().toString();
        String serialised = ctx.newXmlParser().encodeResourceToString(thisone);
        String patient = thisone.getSubject().getReference().toString();
        String docTypeCode = thisone.getType().getCoding().get(0).getCode();
        String docTypeFull = thisone.getType().getCoding().get(0).getSystem() + "|" + thisone.getType().getCoding().get(0).getCode();

        BasicDBObject doc = new BasicDBObject("_id", identifier)
                .append("content", serialised)
                .append("subject", patient)
                .append("typecode", docTypeCode)
                .append("typefull", docTypeFull);

        docRefCollection.update(new BasicDBObject("_id", identifier), doc);
        return identifier;
    }

    /**
     * Code to retrieve a specific document from MongoDB based on identity
     *
     * @param id
     * @return
     */
    @Override
    public DocumentReference getDocRef(String id) {
        DocumentReference foundDocRef = null;
        DBObject foundItem = docRefCollection.findOne(id);
        if(foundItem != null) {
            foundDocRef = (DocumentReference) ctx.newXmlParser().parseResource((String) foundItem.get("content"));
        }
        return foundDocRef;
    }

    /**
     * Code to remove a specific DocRef from MongoDB
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteDocRef(String id) {
        WriteResult res = docRefCollection.remove(new BasicDBObject("_id", id));
        return true;
    }

    /**
     * Code to return all matching DocRefs based on a specified subject
     *
     * @param subjectID
     * @return
     */
    @Override
    public List<DocumentReference> searchDocRefs(String subjectID) {
        List<DocumentReference> list = new ArrayList<DocumentReference>();
        Cursor cursor = docRefCollection.find(new BasicDBObject("subject", subjectID));

        try {
            while(cursor.hasNext()) {
                DocumentReference foundDocRef = (DocumentReference) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
                list.add(foundDocRef);
            }
        } finally {
            cursor.close();
        }
        return list;
    }

    /**
     * Code to search on both Subject and Type
     *
     * @param subjectID
     * @param type
     * @return
     */
    @Override
    public List<DocumentReference> searchDocRefs(String subjectID, String type) {
        List<DocumentReference> list = new ArrayList<DocumentReference>();
        Cursor cursor;
        
        CharSequence cs1 = "|";
        if(type.contains(cs1)) {
            // We're looking for a fully specified type eg:
            // http://snomed.info/sct|861421000000109
            cursor = docRefCollection.find(new BasicDBObject("subject", subjectID).append("typefull", type));
        } else {
            // We're just looking for the code value eg:
            // 861421000000109
            cursor = docRefCollection.find(new BasicDBObject("subject", subjectID).append("typecode", type));
        }
        
        

        try {
            while(cursor.hasNext()) {
                DocumentReference foundDocRef = (DocumentReference) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
                list.add(foundDocRef);
            }
        } finally {
            cursor.close();
        }
        return list;
    }

    /**
     * Code to return ALL docRef objects from our MongoDB database.
     *
     * @return
     */
    @Override
    public List<DocumentReference> getAllDocRefs() {
        List<DocumentReference> list = new ArrayList<DocumentReference>();
        Cursor cursor = docRefCollection.find();

        try {
            while(cursor.hasNext()) {
                DocumentReference foundDocRef = (DocumentReference) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
                list.add(foundDocRef);
            }
        } finally {
            cursor.close();
        }
        return list;
    }

    /**
     * Code to return a specific Organization based on ODS code.
     *
     * @param guid The ODS code
     * @return
     */
    @Override
    public Organization getOrganization(String guid) {
        LOG.info("mongoDatastore.getOrganization(_id) called for: " + guid);
        Organization foundDocRef = null;
        
        DBObject dbObj = orgCollection.findOne(guid);
    
        if(dbObj != null) {
            LOG.info("Org found...");
            foundDocRef = (Organization) ctx.newXmlParser().parseResource((String) dbObj.get("content"));
        } else {
            LOG.info("No Org found, will return null");
        }
        return foundDocRef;
    }

    @Override
    public List<Organization> searchOrgs(String matchValue) {
        LOG.info("mongoDatastore.searchOrgs(matchValue) called.");
        // db.getCollection('Organizations').find({$text:{$search: "DENS"}})
        List<Organization> list = new ArrayList<Organization>();
        
        DBObject findCommand = new BasicDBObject("$text", new BasicDBObject("$search", matchValue));
        Cursor cursor = orgCollection.find(findCommand);

        try {
            while(cursor.hasNext()) {
                Organization foundDocRef = (Organization) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
                list.add(foundDocRef);
            }
        } finally {
            cursor.close();
        }
        return list;
    }

    /**
     * Gets every Organization
     * 
     * @return 
     */
    @Override
    public List<Organization> getAllOrgs() {
        LOG.info("mongoDatastore.getAllOrgs() called.");
        List<Organization> list = new ArrayList<Organization>();
        Cursor cursor = orgCollection.find();

        if(cursor.hasNext()) {
            LOG.info("Got some results");
        }
        
        while(cursor.hasNext()) {
            Organization foundOrg = (Organization) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
            list.add(foundOrg);
            //LOG.info("Org: " + foundOrg.getName());
        }
        cursor.close();
        LOG.log(Level.INFO, "Returning: {0} organizations.", list.size());
        return list;
    }

    @Override
    public List<Organization> getOrgsByODSCode(String ODSCode) {
        LOG.info("mongoDatastore.getOrgsByODSCode([" + ODSCode + "]) called.");
        Organization foundOrg = null;
        List<Organization> list = new ArrayList<Organization>();
        Cursor cursor = orgCollection.find(new BasicDBObject("ods", ODSCode));
        
        if(cursor.hasNext()) {
            LOG.info("Org found...");
            foundOrg = (Organization) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
            list.add(foundOrg);
        } else {
            LOG.info("No Org found, will return null");
        }
        cursor.close();
        return list;
    }

    @Override
    public List<Organization> getOrgsByAddress(String Address) {
        LOG.info("mongoDatastore.getOrgsByAddress([" + Address + "]) called.");
        Organization foundOrg = null;
        List<Organization> list = new ArrayList<Organization>();

        /*
        Cursor cursor = (Cursor) orgCollection.find(new BasicDBObject("$text", new BasicDBObject("$search", Address)));
        
        if(cursor.hasNext()) {
            LOG.info("Org found...");
            foundOrg = (Organization) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
            list.add(foundOrg);
        } else {
            LOG.info("No Org found, will return null");
        }
        cursor.close();
                */
        
        BasicDBObject q = new BasicDBObject();
        q.put("address",  java.util.regex.Pattern.compile(Address));
        Cursor cursor = (Cursor) orgCollection.find(q);
        
        
         if(cursor.hasNext()) {
            LOG.info("Org found...");
            foundOrg = (Organization) ctx.newXmlParser().parseResource((String) cursor.next().get("content"));
            list.add(foundOrg);
        } else {
            LOG.info("No Org found, will return null");
        }
        cursor.close();
        
        return list;
    }
}
