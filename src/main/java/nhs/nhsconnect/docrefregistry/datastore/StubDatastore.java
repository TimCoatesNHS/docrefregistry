/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.datastore;

import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import ca.uhn.fhir.model.primitive.IdDt;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import nhs.nhsconnect.docrefregistry.resources.ResourceFactory;

/**
 * Dummy Datastore to be used for testing. As yet, it doesn't implement search
 * function, nor returning the whole set.
 *
 * @author Tim Coates
 */
public class StubDatastore implements iHAPIDatastore {
    private static final Logger LOG = Logger.getLogger(StubDatastore.class.getName());
    private HashSet db = null;
    
    public String GetName() {
        return "Stub";
    }

    /**
     * Constructor - just creates a HashSet() in which we hold
     *
     */
    public StubDatastore() {
        db = new HashSet();
        LOG.info("StubDatastore created");
    }

    /**
     * Code to simply add an id into a HashMap, in our fake datastore.
     * 
     * @param thisone
     * @return 
     */
    @Override
    public String saveDocRef(DocumentReference thisone) {
        String response = UUID.randomUUID().toString();
        db.add(response);
        LOG.info("saveDocRef called on StubDatasore, record: " + response + " was saved.");
        return response;
    }

    /**
     * Code tp replace the specified item in our fake datastore.
     * 
     * @param thisone
     * @return 
     */
    @Override
    public String updateDocRef(DocumentReference thisone) {
        String result = null;
        String identity;
        if (thisone.getId() != null) {
            identity = thisone.getId().getIdPart();
            if (db.contains(identity)) {
                result = identity;
            }
        }
        return result;
    }

    /**
     * Code to get a specific item from our fake datastore.
     * 
     * @param id
     * @return 
     */
    @Override
    public DocumentReference getDocRef(String id) {
        DocumentReference dr = null;
        if (db.contains(id)) {
            dr = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
            IdDt idDataType = new IdDt(id);
            dr.setId(idDataType);
            dr.setDescription("Fake DocumentReference found from fake Datastore");
        }
        return dr;
    }

    /**
     * Code to delete a specific item from our fake datastore.
     * 
     * @param id
     * @return 
     */
    @Override
    public boolean deleteDocRef(String id) {
        if (db.contains(id)) {
            db.remove(id);
            LOG.info("ID: " + id + " removed from the datastore.");
            return true;
        } else {
            LOG.warning("ID: " + id + " didn't exist in the datastore.");
            return false;
        }
    }

    /**
     * Code to search our fake datastore based on Subject. Simply returns ALL
     * records.
     * 
     * @param subjectID
     * @return 
     */
    @Override
    public List <DocumentReference> searchDocRefs(String subjectID) {
        return getAllDocRefs();
    }
    
    /**
     * Code to search our fake datastore based on subject and type. Just returns
     * all items from our fake datastore.
     * 
     * @param subjectID
     * @param type
     * @return 
     */
    @Override
    public List<DocumentReference> searchDocRefs(String subjectID, String type) {
        return getAllDocRefs();
    }
    
    /**
     * Code to get ALL records from our fake datastore.
     * 
     * @return 
     */
    @Override
    public List <DocumentReference> getAllDocRefs() {
        List<DocumentReference> list = new ArrayList<DocumentReference>();
        
        if (!db.isEmpty()) {
            
            for (Object idObject : db) {
                String identity = idObject.toString();
                DocumentReference dr = new DocumentReference();
                IdDt idDataType = new IdDt(identity);
                dr.setId(idDataType);
                dr.setDescription("Fake DocumentReference: " + identity + " - Pulled from list");
                list.add(dr);
            }
        }
        return list;
    }

    /**
     * Code to get one organisation back from our fake datastore, based on ID.
     * 
     * @param id
     * @return 
     */
    @Override
    public Organization getOrganization(String id) {
        LOG.log(Level.FINE, "StubDatastore.getOrganization( {0} ) called.", id);
        Organization returned = (Organization) ResourceFactory.MakeFake(Organization.class);
        LOG.log(Level.FINE, "Datastore will return: {0}", returned.getName());
        // Push the ID into it...
        IdDt idDataType = new IdDt(id);
        returned.setId(idDataType);
        return returned;
    }

    @Override
    public List<Organization> searchOrgs(String matchValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Organization> getAllOrgs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Organization> getOrgsByODSCode(String ODSCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Organization> getOrgsByAddress(String Address) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
