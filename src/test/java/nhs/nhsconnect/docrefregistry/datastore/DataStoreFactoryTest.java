/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.datastore;

import nhs.nhsconnect.docrefregistry.datastore.DataStoreFactory;
import nhs.nhsconnect.docrefregistry.datastore.iHAPIDatastore;
import nhs.nhsconnect.docrefregistry.datastore.DATASTORETYPES;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tim Coates
 */
public class DataStoreFactoryTest {
    
    //<editor-fold defaultstate="collapsed" desc="Set up etc stuff">
    /**
     * Basic constructor...
     *
     */
    public DataStoreFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    //</editor-fold>

    /**
     * Test of MakeDatastore method, of class DataStoreFactory.
     */
    @Test
    public void testMakeDatastore_enum() {
        System.out.println("MakeDatastore");
        iHAPIDatastore result;
        //iHAPIDatastore result = DataStoreFactory.MakeDatastore(DATASTORETYPES.RETHINK);
        //assertEquals("RethinkDB", result.GetName());
        
        result = DataStoreFactory.MakeDatastore(DATASTORETYPES.STUB);
        assertEquals("Stub", result.GetName());
        
        result = DataStoreFactory.MakeDatastore(DATASTORETYPES.MONGO);
        assertEquals("MongoDB", result.GetName());    
    }    
}
