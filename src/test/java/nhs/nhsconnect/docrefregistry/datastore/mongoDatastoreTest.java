/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhs.nhsconnect.docrefregistry.datastore;

import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import nhs.nhsconnect.docrefregistry.Settings;
import nhs.nhsconnect.docrefregistry.resources.ResourceFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tim.coates@hscic.gov.uk
 */
public class mongoDatastoreTest {

    static mongoDatastore instance;
//<editor-fold defaultstate="collapsed" desc="Set up code etc">

    public mongoDatastoreTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new mongoDatastore(Settings.MongoHost(), Settings.MongoPort());
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
//</editor-fold>

    /**
     * Test of GetName method, of class mongoDatastore.
     */
    @Test
    public void testGetName() {
        System.out.println("GetName");
        String expResult = "MongoDB";
        String result = instance.GetName();
        assertEquals(expResult, result);
    }

    /**
     * Test of saveDocRef method, of class mongoDatastore.
     */
    @Test
    public void testSaveDocRef() {
        System.out.println("saveDocRef");
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);

        String result = instance.saveDocRef(thisone);

        assertNotNull(result);
        assertTrue(result.length() == 36);
    }

    /**
     * Test of updateDocRef method, of class mongoDatastore.
     */
    @Test
    public void testUpdateDocRef() {
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);

        // First we save the DocRef
        String savedID = instance.saveDocRef(thisone);

        // NB We need to set it's Id to the one returned
        thisone.setId(savedID);

        String testValue = "Some specific text to test against";
        thisone.setDescription(testValue);

        // Now we try to update that DocRef...
        String updatedID = instance.updateDocRef(thisone);

        // And check we've got the same ID back
        assertEquals(updatedID, savedID);

        // Now we get the DocRef back, and verify the text was saved in it...
        DocumentReference retrieved = instance.getDocRef(savedID);
        System.out.println("Retrieved DocRef had:\n" + retrieved.getDescription() + "\n\n");

        assertTrue(retrieved.getDescription().equals(testValue));
    }

    /**
     * Test of getDocRef method, of class mongoDatastore.
     */
    @Test
    public void testGetDocRef() {
        System.out.println("getDocRef");
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String checkValue = dateFormat.format(new Date());

        thisone.setDescription(checkValue);

        String expResult = instance.saveDocRef(thisone);

        DocumentReference result = instance.getDocRef(expResult);
        assertEquals(expResult, result.getId().getIdPart());
        assertTrue(checkValue.equals(result.getDescription()));
    }

    /**
     * Test of deleteDocRef method, of class mongoDatastore.
     */
    @Test
    public void testDeleteDocRef() {
        System.out.println("deleteDocRef");
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        String toDelete = instance.saveDocRef(thisone);
        boolean result = instance.deleteDocRef(toDelete);
        assertEquals(true, result);

        DocumentReference shouldntExist = instance.getDocRef(toDelete);
        assertNull(shouldntExist);
    }

    /**
     * Test of searchDocRefs method, of class mongoDatastore.
     */
    @Test
    public void testSearchDocRefs_String() {
        System.out.println("searchDocRefs");

        String subjectID = "http://www.fictional.nhs.uk/fhirserver/Patient/IGNORE";

        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);

        ResourceReferenceDt subj = new ResourceReferenceDt();
        subj.setReference(subjectID);
        subj.setDisplay("Mr Delete Me");
        thisone.setSubject(subj);

        String doc1 = instance.saveDocRef(thisone);
        String doc2 = instance.saveDocRef(thisone);

        List<DocumentReference> result = instance.searchDocRefs(subjectID);

        for (int i = 0; i < result.size(); i++) {
            DocumentReference doc = result.get(i);
            instance.deleteDocRef(doc.getId().getIdPart());
        }

        assertEquals(2, result.size());
        assertTrue(result.get(0).getId().getIdPart().equals(doc1));
        assertTrue(result.get(1).getId().getIdPart().equals(doc2));
    }

    /**
     * Test of searchDocRefs method, of class mongoDatastore.
     */
    @Test
    public void testSearchDocRefs_String_String() {
        System.out.println("searchDocRefs");
        String subjectID = "http://www.fictional.nhs.uk/fhirserver/Patient/IGNORE";

        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);

        ResourceReferenceDt subj = new ResourceReferenceDt();
        subj.setReference(subjectID);
        subj.setDisplay("Mr Delete Me");
        thisone.setSubject(subj);

        String doc1 = instance.saveDocRef(thisone);
        String doc2 = instance.saveDocRef(thisone);

        // Now change the type, and save two more...
        CodingDt docType = new CodingDt();
        docType.setSystem("http://snomed.info/sct")
                .setCode("123")
                .setDisplay("WRONG TYPE");
        thisone.setType(new CodeableConceptDt().addCoding(docType));

        String doc3 = instance.saveDocRef(thisone);
        String doc4 = instance.saveDocRef(thisone);
        
        List<DocumentReference> subjectList = instance.searchDocRefs(subjectID);
        List<DocumentReference> subjectTypeList = instance.searchDocRefs(subjectID, "123");
        List<DocumentReference> subjectTypeList2 = instance.searchDocRefs(subjectID, "http://snomed.info/sct|123");
        
        for (int i = 0; i < subjectList.size(); i++) {
            DocumentReference doc = subjectList.get(i);
            instance.deleteDocRef(doc.getId().getIdPart());
        }

        assertTrue(subjectList.size() > subjectTypeList.size());
        assertEquals(2, subjectTypeList.size());
        assertEquals(2, subjectTypeList2.size());
        assertTrue(subjectTypeList.get(0).getId().getIdPart().equals(doc3));
        assertTrue(subjectTypeList.get(1).getId().getIdPart().equals(doc4));
    }

    /**
     * Test of getAllDocRefs method, of class mongoDatastore.
     */
    @Test
    public void testGetAllDocRefs() {
        System.out.println("getAllDocRefs");

        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String checkValue = dateFormat.format(new Date());

        thisone.setDescription(checkValue);
        String[] docs = new String[6];

        for (int i = 0; i < 6; i++) {
            docs[i] = instance.saveDocRef(thisone);
        }

        List<DocumentReference> result = instance.getAllDocRefs();

        for (int i = 0; i < 6; i++) {
            instance.deleteDocRef(docs[i]);
        }
        assertTrue(result.size() >= 6);
    }

    /**
     * Test of getOrganization method, of class mongoDatastore.
     */
    @Test
    public void testGetOrganization() {
        System.out.println("testGetOrganization");
        Organization org = instance.getOrganization("96eeeb3c-e5d6-4017-a59a-83ca4ce9d478");
        assertTrue(org.getName().equals("PARK MEDICAL GROUP"));
    }

    /**
     * Test of searchOrgs method, of class mongoDatastore.
     */
    @Test
    public void testSearchOrgs() {
        System.out.println("searchOrgs");
        String matchValue = "TH";
        List<Organization> result = instance.searchOrgs(matchValue);
        assertTrue(result.size() >= 2);
    }

    /**
     * Tests getting all Organizations from mongo..
     * 
     */
    @Test
    public void testGetAllOrgs() {
        System.out.println("getAllOrgs");
        List<Organization> result = instance.getAllOrgs();

        assertTrue(result.size() >= 60);
    }

    /**
     * Test of getOrgsByODSCode method, of class mongoDatastore.
     */
    @Test
    public void testGetOrgsByODSCode() {
        System.out.println("getOrgsByODSCode");
        List<Organization> result = instance.getOrgsByODSCode("Y02189");

        assertTrue(result.size() > 0);
    }

    /**
     * Test of getOrgsByAddress method, of class mongoDatastore.
     */
    @Test
    public void testGetOrgsByAddress() {
        System.out.println("getOrgsByAddress");
        List<Organization> result = instance.getOrgsByAddress("LS");

        assertTrue(result.size() > 0);
    }
}
