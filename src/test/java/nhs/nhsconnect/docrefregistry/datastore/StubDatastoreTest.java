/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.datastore;

import nhs.nhsconnect.docrefregistry.datastore.DataStoreFactory;
import nhs.nhsconnect.docrefregistry.datastore.StubDatastore;
import nhs.nhsconnect.docrefregistry.datastore.DATASTORETYPES;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import java.util.List;
import nhs.nhsconnect.docrefregistry.resources.ResourceFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tim Coates
 */
public class StubDatastoreTest {
        
    //<editor-fold defaultstate="collapsed" desc="Set up etc code">
    /**
     * Simple constructor...
     * 
     */
    public StubDatastoreTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    //</editor-fold>

    /**
     * Test of saveDocRef method, of class StubDatastore.
     */
    @Test
    public void testSaveDocRef() {
        System.out.println("saveDocRef");
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        StubDatastore instance = (StubDatastore) DataStoreFactory.MakeDatastore(DATASTORETYPES.STUB);

        String result = instance.saveDocRef(thisone);
        
        assertNotNull(result);
        assertTrue(result.length() == 36);
    }

    /**
     * Test of updateDocRef method, of class StubDatastore.
     */
    @Test
    public void testUpdateDocRef() {
        System.out.println("updateDocRef");
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        
        // First we save a DocRef, getting it's ID value.
        StubDatastore instance = (StubDatastore) DataStoreFactory.MakeDatastore(DATASTORETYPES.STUB);
        String expResult = instance.saveDocRef(thisone);
        thisone.setId(expResult);
        
        // Now we try to update that DocRef...
        String result = instance.updateDocRef(thisone);
        System.out.println("Updated: " + expResult + " and got Result: " + result);
        assertEquals(result, expResult);
    }

    /**
     * Test of findDocumentReference method, of class StubDatastore.
     */
    @Test
    public void testFindDocumentReference() {
        System.out.println("findDocumentReference");
        
        // First we save a DocRef, which we're going to find...
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        StubDatastore instance = (StubDatastore) DataStoreFactory.MakeDatastore(DATASTORETYPES.STUB);
        String id = instance.saveDocRef(thisone);
        
        // We now find it based on the created ID...
        DocumentReference result = instance.getDocRef(id);
        // And check that the found item's ID is the same as what we saved and looked for
        assertTrue(result.getId().toString().equals(id));
        
    }

    /**
     * Test of deleteDocRef method, of class StubDatastore.
     */
    @Test
    public void testDeleteDocRef() {
        System.out.println("deleteDocRef");

        // First we save a DocRef we're going to try to delete...
        DocumentReference thisone = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        StubDatastore instance = (StubDatastore) DataStoreFactory.MakeDatastore(DATASTORETYPES.STUB);
        String id = instance.saveDocRef(thisone);
        // And update the DocRef to have that ID...
        thisone.setId(id);
        boolean result = instance.deleteDocRef(id);
        assertEquals(true, result);
    
        // Now we try again to delete it, where we should get a different result back!
        result = instance.deleteDocRef(id);
        assertEquals(false, result);
    }

    /**
     * Test of searchDocRefs method, of class StubDatastore.
     */
    @Test
    public void testSearchDocRefs() {
        System.out.println("searchDocRefs - Not testable");
    }

    /**
     * Test of getAllDocRefs method, of class StubDatastore.
     */
    @Test
    public void testGetAllDocRefs() {
        System.out.println("getAllDocRefs");
        StubDatastore instance = (StubDatastore) DataStoreFactory.MakeDatastore(DATASTORETYPES.STUB);
        
        DocumentReference first = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        DocumentReference second = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        DocumentReference third = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        DocumentReference fourth = (DocumentReference) ResourceFactory.MakeFake(DocumentReference.class);
        
        String id = instance.saveDocRef(first);
        first.setId(id);
        id = instance.saveDocRef(second);
        second.setId(id);
        
        List <DocumentReference> result = instance.getAllDocRefs();
        assertEquals(result.size(), 2);
        
        id=instance.saveDocRef(third);
        third.setId(id);
        id=instance.saveDocRef(fourth);
        fourth.setId(id);
        
        result = instance.getAllDocRefs();
        assertEquals(result.size(), 4);
    }
}
