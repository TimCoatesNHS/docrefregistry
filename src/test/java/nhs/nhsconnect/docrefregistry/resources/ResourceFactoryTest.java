/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nhs.nhsconnect.docrefregistry.resources;

import nhs.nhsconnect.docrefregistry.resources.ResourceFactory;
import ca.uhn.fhir.model.dstu2.resource.BaseResource;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tim Coates
 */
public class ResourceFactoryTest {
    
//<editor-fold defaultstate="collapsed" desc="Set up stuff">
    public ResourceFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
//</editor-fold>

    /**
     * Test of MakeFake method, of class ResourceFactory.
     */
    @Test
    public void testMakeFake() {
        System.out.println("MakeFake");
        Class type = ca.uhn.fhir.model.dstu2.resource.Organization.class;
        
        //BaseResource expResult = null;
        Organization result1 = (Organization) ResourceFactory.MakeFake(type);
        assertNotNull(result1);
        assertTrue(result1.getName().equals("Bogus GP Practice"));
        assertTrue(result1.getIdentifier().get(0).getValue().equals("ABCD1234"));

        type = ca.uhn.fhir.model.dstu2.resource.DocumentReference.class;
        DocumentReference result2 = (DocumentReference) ResourceFactory.MakeFake(type);
        assertNotNull(result2);
        assertTrue(result2.getDescription().equals("Fake DocumentReference created for test purposes."));
    }
    
}
